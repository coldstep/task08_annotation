import com.task08_annotation.task1.TestAnnotation;
import com.task08_annotation.task2.TestReflectionApi;

public class Test {

  public static void main(String[] args) throws NoSuchMethodException, NoSuchFieldException {
    testTask1();
    testTas21();
  }

  private static void testTask1() throws NoSuchFieldException, NoSuchMethodException {
    System.out.println("Part 1 test custom Annotation");
    TestAnnotation ta = new TestAnnotation("Mark"," Zuckerberg");
    ta.setInfo();
    System.out.println(ta.toString());
    System.out.println(new TestAnnotation("Bill","Gates").toString()) ;
  }

  private static void testTas21() throws NoSuchFieldException, NoSuchMethodException {
    System.out.println("Part 2 test Reflection Api\n myMethod(String... args)");
    TestReflectionApi testApi = new TestReflectionApi();
    testApi.myMethod("Welcome","to","my","world");
    System.out.println("myMethod(String a,int... args)");
    testApi.myMethod("Verification code",2,3,4,5,3);
    System.out.println("\nGet all info about received object:");
    TestAnnotation ta = new TestAnnotation("Mark"," Zuckerberg");
    ta.setInfo();

    testApi.getInfoAboutValue(ta);


  }


}
