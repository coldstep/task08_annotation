package com.task08_annotation.task1;

import java.lang.reflect.Method;

public class TestAnnotation {

  private String name;
  private String surname;
  @CustomField(temp = "20")
  public String age;
  private String profession;
  private String country;
  private String city;


  public TestAnnotation(String name, String surname)
      throws NoSuchFieldException {
    this.name = name;
    this.surname = surname;
    CustomField customField = TestAnnotation.class.getField("age").getAnnotation(CustomField.class);
    age = customField.temp();
  }

  @CustomMethod()
  public void setInfo() throws NoSuchMethodException {
    Method method = TestAnnotation.class.getMethod("setInfo");
    CustomMethod ca = method.getAnnotation(CustomMethod.class);
    profession = ca.profession();
    country = ca.country();
    city = ca.city();
  }

  @Override
  public String toString() {
    return "TestAnnotation{" +
        "Name='" + name + '\'' +
        ", Surname='" + surname + '\'' +
        ", Age='" + age + '\'' +
        ", Profession='" + profession + '\'' +
        ", Country='" + country + '\'' +
        ", City='" + city + '\'' +
        '}';
  }
}
