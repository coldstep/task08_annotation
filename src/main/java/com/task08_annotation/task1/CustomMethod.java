package com.task08_annotation.task1;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD})
public @interface CustomMethod {

  String profession() default "IT specialist";

  String country() default "USA";

  String city() default "San Francisco";
}
