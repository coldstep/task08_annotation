package com.task08_annotation.task2;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;


public class TestReflectionApi {

  public void myMethod(String... args) {
    System.out.println(Arrays.asList(args));
  }

  public void myMethod(String a, int... args) {
    System.out.println(a + Arrays.asList(args));
    System.out.printf(a + " ");
    for (int i : args) {
      System.out.printf(i + " ");
    }
  }

  public void getInfoAboutValue(Object obj) {
    StringBuilder builder = new StringBuilder();
    Class cls = obj.getClass();
    builder.append("All information about ")
        .append(cls.getSimpleName()).append("\n")
        .append("Simple name - ").append(cls.getSimpleName()).append("\n")
        .append("Package - ").append(cls.getPackage()).append("\n")
        .append("IsAnonymousClass - ").append(cls.isAnonymousClass()).append("\n")
        .append("IsLocalClass - ").append(cls.isLocalClass()).append("\n")
        .append("IsMemberClass - ").append(cls.isMemberClass()).append("\n")
        .append("IsAnnotation - ").append(cls.isAnnotation()).append("\n")
        .append("IsPrimitive - ").append(cls.isPrimitive()).append("\n")
        .append("IsArray - ").append(cls.isArray()).append("\n")
        .append("IsEnum - ").append(cls.isEnum()).append("\n")
        .append("IsInterface - ").append(cls.isInterface()).append("\n")
        .append("IsAnnotation - ").append(cls.isAnnotation()).append("\n")
    ;
    Field[] fields = cls.getDeclaredFields();
    builder.append("[--List of declared fields:--]\n");
    for (Field i : fields) {
      builder.append(i).append("\n");
    }
    Constructor[] constructors = cls.getDeclaredConstructors();
    builder.append("[--List of declared constructors:--]\n");
    for (Constructor i : constructors) {
      builder.append(i).append("\n");
    }
    Method[] methods = cls.getDeclaredMethods();
    builder.append("[--List of declared methods:--]\n");
    for (Method i : methods) {
      builder.append(i).append("\n");
    }
    Annotation[] annotations = cls.getAnnotations();
    builder.append("[--List of declared annotation:--]\n");
    for (Annotation i : annotations) {
      builder.append(i).append("\n");
    }

    System.out.println(builder.toString());
  }
}
